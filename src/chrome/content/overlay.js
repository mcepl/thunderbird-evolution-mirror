var eMirrorNotify = {
	
	setupCalendar : function eMirrorNotify_setupCalendar() {
	
		//First run stuff
		var prefs = Components.classes["@mozilla.org/preferences-service;1"]
                        .getService(Components.interfaces.nsIPrefService);
		var firstrun = prefs.getBoolPref("extensions.evolutionmirror.firstrun");
		if (firstrun) {
			prefs.setBoolPref("extensions.evolutionmirror.firstrun", false);
			//get all the items from all calendars and add them to EDS
			for each (var aCalendar in getCalendarManager().getCalendars({})) {
				var getListener = {
		
					onOperationComplete: function (aCalendar, aStatus, aOperationType, aId, aDetail) {	},
			
					onGetResult: function (aCalendar, aStatus, aItemType, aDetail, aCount, aItems) {
						for each (var item in aItems) {
							eMirrorNotify.addEvent(item);
						}
					}
				};
		
				aCalendar.getItems(Components.interfaces.calICalendar.ITEM_FILTER_ALL_ITEMS, 0, null, null, getListener);
			}
		}
	
		// setting up listeners etc
		if (this.calendar == null) {
			this.calendar = getCompositeCalendar();
		}
		if (this.calendar) {
			this.calendar.removeObserver(this.calendarObserver);
		}
		this.calendar.addObserver(this.calendarObserver);
		if (this.mListener) {
			this.mListener.updatePeriod();
		}
    },
	
	calendarOpListener : {
		onOperationComplete : function listener_onOperationComplete(calendarObserver, status, optype, id, detail) {	},
		onGetResult : function listener_onGetResult(calendarObserver, status, itemtype, detail, count, items) {
			if (!Components.isSuccessCode(status)) {
				return;
			}
			items.forEach(eMirrorNotify.addItem, eMirrorNotify);
		}
	},
	
	calendarObserver : {
		QueryInterface : function eMirrorNotify_QI(aIID) {
			if (!aIID.equals(Components.interfaces.calIObserver) &&
				!aIID.equals(Components.interfaces.calICompositeObserver) &&
				!aIID.equals(Components.interfaces.nsISupports)) {
				throw Components.results.NS_ERROR_NO_INTERFACE;
			}
			return this;
		},
		
		onAddItem : function eMirrorNotify_onAddItem(item) {
			eMirrorNotify.addEvent(item);
			
		},
		
		onDeleteItem : function eMirrorNotify_onDeleteItem(item, rebuildFlag) {
			eMirrorNotify.deleteEvent(item);
			
		},
		
		onModifyItem : function eMirrorNotify_onModifyItem(newItem, oldItem) {
			eMirrorNotify.deleteEvent(oldItem);
			
			eMirrorNotify.addEvent(newItem);
			
		},
		
		onCalendarAdded : function eMirrorNotify_calAdd(aCalendar) {
			//This is called when a new calendar is added.
			//We can get all the items from the calendar and add them one by one to Evolution Data Server
	
			var getListener = {
		
				onOperationComplete: function(aCalendar, aStatus, aOperationType, aId, aDetail) { },
		
				onGetResult: function(aCalendar, aStatus, aItemType, aDetail, aCount, aItems) {
					for each (item in aItems) {
						eMirrorNotify.addEvent(item);
					}
				}
			};
	
			aCalendar.getItems(Components.interfaces.calICalendar.ITEM_FILTER_ALL_ITEMS, 0, null, null, getListener);
		},
		
		onCalendarRemoved : function eMirrorNotify_calRemove(aCalendar) {
	
			var getListener = {
		
				onOperationComplete: function(aCalendar, aStatus, aOperationType, aId, aDetail) { },
		
				onGetResult: function(aCalendar, aStatus, aItemType, aDetail, aCount, aItems) {
					for each (item in aItems) {
						eMirrorNotify.deleteEvent(item);
					}
				}
			};
	
			aCalendar.getItems(Components.interfaces.calICalendar.ITEM_FILTER_ALL_ITEMS, 0, null, null, getListener);
		},
		
		onStartBatch : function eMirrorNotify_onStartBatch() {
			this.mBatchCount++;
		},
		
		onEndBatch : function() {
			this.mBatchCount--;
			if (this.mBatchCount == 0) {
				eMirrorNotify.refreshCalendarQuery();
			}
		},
		
		onError : function() { },
		onPropertyChanged : function() { },
		onPropertyDeleting : function() { },
		onDefaultCalendarChanged : function() { },
		onLoad : function() { }
	},
	
	addEvent: function addEvent(item) {
		let comp = libical.icalcomponent_new_from_string(item.icalString);
		let uid = glib.gchar.ptr();
		let error = glib.GError.ptr();
		let error2 = glib.GError.ptr();
		this.ecal = libecal.e_cal_new_system_calendar();
		
		// TODO If evolution has never been run, opening the system calendar fails.
        if (libecal.e_cal_open(this.ecal, true, error.address())) { } else {
			eMirrorNotify.debug("Error opening EDS Calendar: " + 
					error.contents.code + " - " +
					error.contents.message.readString());;
		}

		let itemcomp = this.vcalendar_add_timezones_get_item(comp);
		rc = libecal.e_cal_create_object(this.ecal, itemcomp, uid.address(), error2.address());
			
	},
	
	deleteEvent: function deleteEvent(item) {
		let error = glib.GError.ptr();
		let error2 = glib.GError.ptr();
		this.ecal = libecal.e_cal_new_system_calendar();
		
		// TODO If evolution has never been run, opening the system calendar fails.
        if (libecal.e_cal_open(this.ecal, true, error.address())) { } else {
			eMirrorNotify.debug("Error opening EDS Calendar: " + 
					error.contents.code + " - " +
					error.contents.message.readString());;
		}

		rc = libecal.e_cal_remove_object(this.ecal, item.id, error2.address());
		
	},
	
    vcalendar_add_timezones_get_item: function vcalendar_add_timezones(comp) {
        let error = glib.GError.ptr();
        let subcomp = libical.icalcomponent_get_first_component(comp, libical.ICAL_ANY_COMPONENT);
        let itemcomp;
        while (!subcomp.isNull()) {
            switch (libical.icalcomponent_isa(subcomp)) {

                case libical["ICAL_VTIMEZONE_COMPONENT"]: {
                    let zone = libical.icaltimezone_new();
                    if (libical.icaltimezone_set_component(zone, subcomp)) {
                        libecal.e_cal_add_timezone(this.ecal, zone, error.address());
                    }
                    break;
                }
                case libical["ICAL_VEVENT_COMPONENT"]:
                case libical["ICAL_VTODO_COMPONENT"]:
                case libical["ICAL_VJOURNAL_COMPONENT"]:
                    itemcomp = subcomp;
                    break;
            }
            subcomp = libical.icalcomponent_get_next_component(comp, libical.ICAL_ANY_COMPONENT);
        }

        return itemcomp;
    },

	init: function() { 
		glib.init();
		libical.init();
		libecal.init();
		this.setupCalendar();
	},
	
	uninit: function() {
		libecal.shutdown();
		libical.shutdown();
		glib.shutdown();
	},
	
	debug: function (aMessage) {
		var consoleService = Components.classes["@mozilla.org/consoleservice;1"]
			.getService(Components.interfaces.nsIConsoleService);
		consoleService.logStringMessage("Evolution Mirror Extension (" + new Date() + " ):\n\t" + aMessage);
		window.dump("Evolution Mirror Extension: (" + new Date() + " ):\n\t" + aMessage + "\n");
	}
};

window.addEventListener("load", function() {eMirrorNotify.init()}, false);
window.addEventListener("unload", function() {eMirrorNotify.uninit()}, false);
