var libecal = {
    
    ecallibPath: ["libecal-1.2.so.10", 
                  "libecal-1.2.so.11"],

    lib: null,
    
    init: function() {
        
        Components.utils.import("resource://gre/modules/ctypes.jsm");

        for (path in this.ecallibPath) {
            try {
                this.lib = ctypes.open(this.ecallibPath[path]);
                this.debug("Opened " + this.ecallibPath[path]);
                break;
            } catch (err) {
                this.debug("Failed to open " + this.ecallibPath[path] + ": " + err);
            }
        }

        //Enum
        this.CalObjModType = ctypes.int;
        this.CALOBJ_MOD_THIS = 1;
        this.CALOBJ_MOD_THISANDPRIOR = 2;
        this.CALOBJ_MOD_THISANDFUTURE = 4;
        this.CALOBJ_MOD_ALL = 7;
    
        //Structures
        this._ECal = new ctypes.StructType("_ECal"); //, [{object: this.GObject}, {priv: this.ECalPrivate.ptr}]);
        this.ECal = this._ECal;
    
        //Methods
        this.e_cal_open                   = this.lib.declare("e_cal_open", 
                                                        ctypes.default_abi, 
                                                        glib.gboolean, 
                                                        this.ECal.ptr, 
                                                        glib.gboolean, 
                                                        glib.GError.ptr.ptr);
                                                        
        this.e_cal_new_system_calendar    = this.lib.declare("e_cal_new_system_calendar", 
                                                        ctypes.default_abi, 
                                                        this.ECal.ptr);
 
        this.e_cal_get_object_list        = this.lib.declare("e_cal_get_object_list", 
                                                        ctypes.default_abi, 
                                                        glib.gboolean, 
                                                        this.ECal.ptr, 
                                                        glib.gchar.ptr, 
                                                        glib.GList.ptr.ptr, 
                                                        glib.GError.ptr.ptr);
                                                        
        this.e_cal_free_object_list       = this.lib.declare("e_cal_free_object_list", 
                                                        ctypes.default_abi, 
                                                        ctypes.void_t, 
                                                        glib.GList.ptr);

        this.e_cal_create_object          = this.lib.declare("e_cal_create_object", 
                                                        ctypes.default_abi, 
                                                        glib.gboolean, 
                                                        this.ECal.ptr, 
                                                        libical.icalcomponent.ptr, 
                                                        glib.gchar.ptr.ptr, 
                                                        glib.GError.ptr.ptr);
                                                        
        this.e_cal_modify_object          = this.lib.declare("e_cal_modify_object", 
                                                        ctypes.default_abi, 
                                                        glib.gboolean, 
                                                        this.ECal.ptr, 
                                                        libical.icalcomponent.ptr, 
                                                        this.CalObjModType, 
                                                        glib.GError.ptr.ptr);

        this.e_cal_remove_object          = this.lib.declare("e_cal_remove_object", 
                                                        ctypes.default_abi, 
                                                        glib.gboolean, 
                                                        this.ECal.ptr, 
                                                        glib.gchar.ptr, 
                                                        glib.GError.ptr.ptr);
                                                        
        this.e_cal_remove_object_with_mod = this.lib.declare("e_cal_remove_object_with_mod", 
                                                        ctypes.default_abi, 
                                                        glib.gboolean, 
                                                        this.ECal.ptr, 
                                                        glib.gchar.ptr, 
                                                        glib.gchar.ptr, 
                                                        this.CalObjModType, 
                                                        glib.GError.ptr.ptr);

        this.e_cal_add_timezone           = this.lib.declare("e_cal_add_timezone", 
                                                        ctypes.default_abi, 
                                                        glib.gboolean, 
                                                        this.ECal.ptr, 
                                                        libical.icaltimezone.ptr, 
                                                        glib.GError.ptr.ptr);

    },
    
    debug: function (aMessage) {
        var consoleService = Components.classes["@mozilla.org/consoleservice;1"]
            .getService(Components.interfaces.nsIConsoleService);
        consoleService.logStringMessage("Ecallib (" + new Date() + " ):\n\t" + aMessage);
        window.dump("Ecallib: (" + new Date() + " ):\n\t" + aMessage + "\n");
    },
    
    shutdown: function() {
        this.lib.close();
    }
}
