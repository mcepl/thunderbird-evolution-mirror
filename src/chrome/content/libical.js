var libical = {
	
	icallibPath: "libical.so.0",
	
	lib: null,
	
	init: function() {
		
		Components.utils.import("resource://gre/modules/ctypes.jsm");

		this.lib = ctypes.open(this.icallibPath);
		
		//Enum
		this.icalcomponent_kind = ctypes.int; 
        this.ICAL_NO_COMPONENT = 0;
        this.ICAL_ANY_COMPONENT = 1;
        this.ICAL_XROOT_COMPONENT = 2;
        this.ICAL_XATTACH_COMPONENT = 3;
        this.ICAL_VEVENT_COMPONENT = 4;
        this.ICAL_VTODO_COMPONENT = 5;
        this.ICAL_VJOURNAL_COMPONENT = 6;
        this.ICAL_VCALENDAR_COMPONENT = 7;
        this.ICAL_VAGENDA_COMPONENT = 8;
        this.ICAL_VFREEBUSY_COMPONENT = 9;
        this.ICAL_VALARM_COMPONENT = 10;
        this.ICAL_XAUDIOALARM_COMPONENT = 11;
        this.ICAL_XDISPLAYALARM_COMPONENT = 12;
        this.ICAL_XEMAILALARM_COMPONENT = 13;
        this.ICAL_XPROCEDUREALARM_COMPONENT = 14;
        this.ICAL_VTIMEZONE_COMPONENT = 15;
        this.ICAL_XSTANDARD_COMPONENT = 16;
        this.ICAL_XDAYLIGHT_COMPONENT = 17;
        this.ICAL_X_COMPONENT = 18;
        this.ICAL_VSCHEDULE_COMPONENT = 19;
        this.ICAL_VQUERY_COMPONENT = 20;
        this.ICAL_VREPLY_COMPONENT = 21;
        this.ICAL_VCAR_COMPONENT = 22;
        this.ICAL_VCOMMAND_COMPONENT = 23;
        this.ICAL_XLICINVALID_COMPONENT = 24;
        this.ICAL_XLICMIMEPART_COMPONENT = 25;
    
		//Structures
        this.icalcomponent_impl = new ctypes.StructType("icalcomponent_impl");
        this.icalcomponent = this.icalcomponent_impl;

        this._icaltimezone = new ctypes.StructType("_icaltimezone");
        this.icaltimezone = this._icaltimezone;

		//Methods
        this.icalcomponent_as_ical_string      = this.lib.declare("icalcomponent_as_ical_string", 
                                                          ctypes.default_abi, 
                                                          ctypes.char.ptr, 
                                                          this.icalcomponent.ptr);
                                                          
        this.icalcomponent_as_ical_string_r    = this.lib.declare("icalcomponent_as_ical_string_r", 
                                                          ctypes.default_abi, 
                                                          ctypes.char.ptr, 
                                                          this.icalcomponent.ptr);

        this.icalcomponent_new_from_string     = this.lib.declare("icalcomponent_new_from_string", 
                                                          ctypes.default_abi, 
                                                          this.icalcomponent.ptr, 
                                                          ctypes.char.ptr);
                                                          
        this.icalcomponent_get_first_component = this.lib.declare("icalcomponent_get_first_component", 
                                                          ctypes.default_abi, 
                                                          this.icalcomponent.ptr, 
                                                          this.icalcomponent.ptr, 
                                                          this.icalcomponent_kind);
                                                          
        this.icalcomponent_get_next_component  = this.lib.declare("icalcomponent_get_next_component", 
                                                          ctypes.default_abi, 
                                                          this.icalcomponent.ptr, 
                                                          this.icalcomponent.ptr, 
                                                          this.icalcomponent_kind);
                                                          
        this.icalcomponent_isa                 = this.lib.declare("icalcomponent_isa", 
                                                          ctypes.default_abi, 
                                                          this.icalcomponent_kind, 
                                                          this.icalcomponent.ptr);

        this.icaltimezone_set_component        = this.lib.declare("icaltimezone_set_component", 
                                                          ctypes.default_abi, 
                                                          ctypes.int, 
                                                          this.icaltimezone.ptr, 
                                                          this.icalcomponent.ptr);
                                                          
        this.icaltimezone_new                  = this.lib.declare("icaltimezone_new", 
                                                          ctypes.default_abi, 
                                                          this.icaltimezone.ptr);

	},
	
	debug: function (aMessage) {
		var consoleService = Components.classes["@mozilla.org/consoleservice;1"]
			.getService(Components.interfaces.nsIConsoleService);
		consoleService.logStringMessage("Icallib (" + new Date() + " ):\n\t" + aMessage);
		window.dump("Icallib: (" + new Date() + " ):\n\t" + aMessage + "\n");
	},
	
	shutdown: function() {
		this.lib.close();
	}
}
