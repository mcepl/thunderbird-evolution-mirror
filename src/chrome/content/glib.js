var glib = {
	
	glibPath: "libglib-2.0.so.0",
	
	lib: null,
	
	init: function() {
		
		Components.utils.import("resource://gre/modules/ctypes.jsm");

		this.lib = ctypes.open(this.glibPath);
		
		//Structures
		this.GQuark = ctypes.uint32_t;
        this.gchar = ctypes.char;
        this.gshort = ctypes.short;
        this.glong = ctypes.long;
        this.gint = ctypes.int;
        this.gboolean = this.gint;
        this.guchar = ctypes.unsigned_char;
        this.gushort = ctypes.unsigned_short;
        this.gulong = ctypes.unsigned_long;
        this.guint = ctypes.unsigned_int;
        this.gfloat = ctypes.float;
        this.gdouble = ctypes.double;
        this.gpointer = ctypes.void_t.ptr;
        this.gconstpointer = ctypes.void_t.ptr;

        this._GError = new ctypes.StructType("_GError", [{domain: this.GQuark}, 
                                                         {code: this.gint}, 
                                                         {message: this.gchar.ptr}]);
        this.GError = this._GError;
        
		this._GList = new ctypes.StructType("_Glist");
        this._GList.define([{data: this.gpointer}, 
                            {next: this._GList.ptr}, 
                            {prev: this._GList.ptr}]);
        this.GList = this._GList;

		//Methods
        this.g_list_alloc   = this.lib.declare("g_list_alloc", 
                                          ctypes.default_abi, 
                                          this.GList.ptr);
                                          
        this.g_list_free    = this.lib.declare("g_list_free", 
                                          ctypes.default_abi, 
                                          ctypes.void_t, 
                                          this.GList.ptr);
                                          
        this.g_list_free_1  = this.lib.declare("g_list_free_1", 
                                          ctypes.default_abi, 
                                          ctypes.void_t, 
                                          this.GList.ptr);
                                          
        this.g_list_length  = this.lib.declare("g_list_length", 
                                          ctypes.default_abi, 
                                          this.guint, 
                                          this.GList.ptr);
				
	},
	
	debug: function (aMessage) {
		var consoleService = Components.classes["@mozilla.org/consoleservice;1"]
			.getService(Components.interfaces.nsIConsoleService);
		consoleService.logStringMessage("GLib (" + new Date() + " ):\n\t" + aMessage);
		window.dump("GLib: (" + new Date() + " ):\n\t" + aMessage + "\n");
	},
	
	shutdown: function() {
		this.lib.close();
	}
}
