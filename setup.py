#!/usr/bin/python
#

"""
Setup file to package Evolution Mirror
"""

from distutils.core import setup
from DistUtilsExtra.command import build_extra
import shutil
import os

shutil.make_archive("{531b5d09-9f8e-452b-8681-2fc03c9c150e}.xpi", "zip", "src")
os.rename("{531b5d09-9f8e-452b-8681-2fc03c9c150e}.xpi.zip", "{531b5d09-9f8e-452b-8681-2fc03c9c150e}.xpi")

setup(name="xul-ext-evolution-mirror",
      version="0.3.2",
      author="Mark Tully",
      author_email="markjtully@gmail.com",
      url="https://addons.mozilla.org/en-US/thunderbird/addon/evolution-mirror/",
      license="GNU General Public License Version 2 (GPLv2)",
      data_files=[
        ("/usr/lib/mozilla/extensions/{3550f703-e582-4d05-9a08-453d09bdfdc6}", ['{531b5d09-9f8e-452b-8681-2fc03c9c150e}.xpi', ]),
      ],
      cmdclass={"build": build_extra.build_extra, }
)
